#ifndef DATATYPE_H
#define DATATYPE_H

typedef struct autic {
	int id;
	char marka[30];
	char model[30];
	char cijena[10];					
	char godinaProizvodnje[10]; 
	char maxBrzina[10];			
	char snaga[10];				
	char motor[30];
	char vrsta[30];
	char potrosnja[30];
	char masa[15];				
	char gorivo[15];
	char prijenos[15];
	char pogon[15];
}AUTO;

#endif //DATATYPE_H