#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"

int brojAuta = 0;
//int mic = 0;
void kreiranjeDatoteke(const char* const imeDatoteke) {
	FILE* pF = fopen(imeDatoteke, "wb");
	if (pF == NULL) {
		perror("Kreiranje datoteke");
		exit(EXIT_FAILURE);
	}
	fwrite(&brojAuta, sizeof(int), 1, pF);
	fclose(pF);
}

void dodajAuto(const char* const imeDatoteke) {
	FILE* pF = fopen(imeDatoteke, "rb+");
	if (pF == NULL) {
		perror("Dodavanje auta u datoteke");
		exit(EXIT_FAILURE);
	}
	fread(&brojAuta, sizeof(int), 1, pF);
	printf("brojAuta: %d\n", brojAuta);			
	AUTO temp = { 0 };
	temp.id = brojAuta;
	getchar();
	printf("Unesite marku auta!\n");
	scanf("%19[^\n]", temp.marka);
	getchar();
	printf("Unesite model auta!\n");
	scanf("%19[^\n]", temp.model);
	getchar();										
	printf("Unesite cijenu auta!\n");
	scanf("%19[^\n]", temp.cijena);
	getchar();
	printf("Unesite godinu proizvodnje auta!\n");
	scanf("%19[^\n]", temp.godinaProizvodnje);
	getchar();
	printf("Unesite maksimalnu brzinu auta!\n");
	scanf("%19[^\n]", temp.maxBrzina);
	getchar();
	printf("Unesite snagu auta!\n");
	scanf("%19[^\n]", temp.snaga);
	getchar();
	printf("Unesite vrstu motora auta!\n");
	scanf("%19[^\n]", temp.motor);
	getchar();
	printf("Unesite vrstu auta!\n");
	scanf("%19[^\n]", temp.vrsta);
	getchar();
	printf("Unesite potrosnju auta!\n");
	scanf("%19[^\n]", temp.potrosnja);
	getchar();
	printf("Unesite masu auta!\n");
	scanf("%19[^\n]", temp.masa);
	getchar();
	printf("Unesite vrstu goriva auta!\n");
	scanf("%19[^\n]", temp.gorivo);
	getchar();
	printf("Unesite vrstu prijenosa auta!\n");
	scanf("%19[^\n]", temp.prijenos);
	getchar();
	printf("Unesite vrstu pogona auta!\n");
	scanf("%19[^\n]", temp.pogon);
	fseek(pF, sizeof(AUTO) * brojAuta, SEEK_CUR);
	fwrite(&temp, sizeof(AUTO), 1, pF);
	rewind(pF);
	brojAuta++;
	fwrite(&brojAuta, sizeof(int), 1, pF);
	fclose(pF);
}

void* ucitavanjeAuta(const char* const imeDatoteke) {
	FILE* pF = fopen(imeDatoteke, "rb");
	if (pF == NULL) {
		perror("Ucitavanje auta iz datoteke");
		return NULL;
		//exit(EXIT_FAILURE);
	}
	fread(&brojAuta, sizeof(int), 1, pF);
	printf("brojAuta: %d\n", brojAuta);						
	AUTO* poljeAuta = (AUTO*)calloc(brojAuta, sizeof(AUTO));
	if (poljeAuta == NULL) {
		perror("Zauzimanje memorije za aute");
		return NULL;
		//exit(EXIT_FAILURE);
	}
	fread(poljeAuta, sizeof(AUTO), brojAuta, pF);
	return poljeAuta;
}

void ispisivanjeAuta(const AUTO* const poljeAuta) {
	if (poljeAuta == NULL) {
		printf("Polje auta je prazno!\n");
		return;
	}
	int i;
	for (i = 0; i < brojAuta; i++)
	{
		if (i != (poljeAuta + i)->id) {
			printf("Niste ucitali novi auto!\n\n");
			return;
		}
		printf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
		printf("Auto broj %d\nID: %d\nMarka: %s\nModel: %s\n\Cijena: %skn\nGodina proizvodnje: %s\nMaksimalna brzina: %skm/h\nSnaga: %sks\nMotor: %s\nVrsta: %s\nPotrosnja(po 100km): %s\nMasa: %skg\nGorivo: %s\nPrijenos: %s\nPogon: %s\n",
			i + 1,
			(poljeAuta + i)->id,
			(poljeAuta + i)->marka,
			(poljeAuta + i)->model,
			(poljeAuta + i)->cijena,
			(poljeAuta + i)->godinaProizvodnje,
			(poljeAuta + i)->maxBrzina,
			(poljeAuta + i)->snaga,
			(poljeAuta + i)->motor,
			(poljeAuta + i)->vrsta,
			(poljeAuta + i)->potrosnja,
			(poljeAuta + i)->masa,
			(poljeAuta + i)->gorivo,
			(poljeAuta + i)->prijenos,
			(poljeAuta + i)->pogon);
		printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n");
	}
}

void* pretrazivanjeAuta(AUTO* const poljeAuta) {
	if (poljeAuta == NULL) {
		printf("Polje auta je prazno!\n");
		return NULL;
	}
	int i, ima = 0;
	int trazeniID;
	printf("Unesite ID auta koji zelite izdvojiti.\n");
	getchar();
	scanf("%d", &trazeniID);
	for (i = 0; i < brojAuta; i++)
	{
		if (trazeniID == (poljeAuta + i)->id) {		
			ima += 1;
			printf("Ima podudaranja!\n\n");
			for (i = trazeniID; i <= trazeniID; i++)
			{
				printf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
				printf("Auto broj %d\nID: %d\nMarka: %s\nModel: %s\n\Cijena: %s kn\nGodina proizvodnje: %s\nMaksimalna brzina: %s km/h\nSnaga: %s ks\nMotor: %s\nVrsta: %s\nPotrosnja(po 100km): %s\nMasa: %s kg\nGorivo: %s\nPrijenos: %s\nPogon: %s\n",
					i + 1,
					(poljeAuta + i)->id,
					(poljeAuta + i)->marka,
					(poljeAuta + i)->model,
					(poljeAuta + i)->cijena,
					(poljeAuta + i)->godinaProizvodnje,
					(poljeAuta + i)->maxBrzina,
					(poljeAuta + i)->snaga,
					(poljeAuta + i)->motor,
					(poljeAuta + i)->vrsta,
					(poljeAuta + i)->potrosnja,
					(poljeAuta + i)->masa,
					(poljeAuta + i)->gorivo,
					(poljeAuta + i)->prijenos,
					(poljeAuta + i)->pogon);
				printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n");
			}
			return (poljeAuta + i);
		}
	}
	if (ima == 0) {
		printf("Nema podudaranja!\n");
	}
	return NULL;
}

void promjenaImenaDatoteci(const char* staroImeDatoteke) {
	char novoImeDatoteke[20] = { '\0' };
	printf("Unesite novi naziv datoteke!\n");
	getchar();
	scanf("%19[^\n]", novoImeDatoteke);
	printf("Zelite li uistinu promijeniti ime datoteci?\n");
	printf("Utipkajte \"da\" ako uistinu zelite promijeniti ime datoteke u suprotno utipkajte \"ne\"!\n");
	char potvrda[3] = { '\0' };
	scanf("%2s", potvrda);
	if (!strcmp("da", potvrda)) {
		if (rename(staroImeDatoteke, novoImeDatoteke) == 0) {
			printf("Uspjesno promijenjeno ime datoteci!\n");
			printf("\nMolim iza�ite iz programa sada.\n");
		}
		else {
			printf("Neuspjesno promijenjeno ime datoteci!\n");
		}
	}
}

void brisanjeAuta(AUTO** const trazeniAuto, const AUTO* const poljeAuta, const char* const imeDatoteke) {
	FILE* pF = fopen(imeDatoteke, "wb");
	if (pF == NULL) {
		perror("Brisanje auta iz datoteke");
		exit(EXIT_FAILURE);
	}
	fseek(pF, sizeof(int), SEEK_SET);
	int i;
	int noviBrojacAuta = 0;
	brojAuta -= 1;
	for (i = 0; i < brojAuta; i++)
	{
		if (*trazeniAuto != (poljeAuta + i)) {
			fwrite((poljeAuta + i), sizeof(AUTO), 1, pF);
			noviBrojacAuta++;
		}
	}
	rewind(pF);
	fwrite(&noviBrojacAuta, sizeof(int), 1, pF);
	fclose(pF);
	printf("Posljednji unesen auto je uspjesno obrisan!\n");
	*trazeniAuto = NULL;
}

void brisanjeDatoteke(const char* imeDatoteke) {
	printf("Zelite li uistinu obrisati datoteku %s?\n", imeDatoteke);
	printf("Utipkajte \"da\" ako uistinu zelite obrisati datoteku u suprotno utipkajte\"ne\"!\n");
	char potvrda[3] = { '\0' };
	scanf("%2s", potvrda);
	if (!strcmp("da", potvrda)) {
		remove(imeDatoteke) == 0 ? printf("Uspjesno obrisana datoteka %s!\n",
			imeDatoteke) : printf("Neuspjesno brisanje datoteke %s!\n", imeDatoteke);
	}
}

int izlazIzPrograma(AUTO* poljeAuta) {
	free(poljeAuta);
	return 0;
}