#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "dataType.h"
#include "header.h"

int izbornik(const char* const imeDatoteke) {
	printf("====================");
	printf("Odaberite jednu od ponudenih opcija:");
	printf("====================\n");
	printf("\t\t\tOpcija 1: kreiranje datoteke!\n");
	printf("\t\t\tOpcija 2: dodavanje auta!\n");
	printf("\t\t\tOpcija 3: ucitavanje auta!\n");
	printf("\t\t\tOpcija 4: ispisivanje auta!\n");
	printf("\t\t\tOpcija 5: pretrazivanje auta po imenu!\n");
	printf("\t\t\tOpcija 6: brisanje posljednje unesenog auta!\n");
	printf("\t\t\tOpcija 7: promjena imena datoteci!\n");
	printf("\t\t\tOpcija 8: brisanje datoteke!\n");
	printf("\t\t\tOpcija 9: izlaz iz programa!\n");
	printf("======================================\
======================================\n");

	int uvijet = 0;
	static AUTO* poljeAuta = NULL;
	static AUTO* pronadeniAuti = NULL;
	scanf("%d", &uvijet);
	switch (uvijet) {
	case 1:
		kreiranjeDatoteke(imeDatoteke);
		break;
	case 2:
		dodajAuto(imeDatoteke);
		break;
	case 3:
		if (poljeAuta != NULL) {
			free(poljeAuta);
			poljeAuta = NULL;
		}
		poljeAuta = (AUTO*)ucitavanjeAuta(imeDatoteke);
		if (poljeAuta == NULL) {
			exit(EXIT_FAILURE);
		}
		break;
	case 4:
		ispisivanjeAuta(poljeAuta);
		break;
	case 5:
		pronadeniAuti = (AUTO*)pretrazivanjeAuta(poljeAuta);
		break;
	case 6:
		brisanjeAuta(&pronadeniAuti, poljeAuta, imeDatoteke);
		break;
	case 7:
		promjenaImenaDatoteci(imeDatoteke);
		break;
	case 8:
		brisanjeDatoteke(imeDatoteke);
		break;
	case 9:
		uvijet = izlazIzPrograma(poljeAuta);
		break;
	default:
		uvijet = 0;
	}
	return uvijet;
}