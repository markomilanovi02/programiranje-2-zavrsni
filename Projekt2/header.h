#ifndef FUNCTIONS_C
#define FUNCTIONS_C
#include "dataType.h"

int izbornik(const char* const);
void kreiranjeDatoteke(const char* const);
void dodajAuto(const char* const);
void* ucitavanjeAuta(const char* const);
void ispisivanjeAuta(const AUTO* const);
void* pretrazivanjeAuta(AUTO* const);
void brisanjeAuta(AUTO** const, const AUTO* const, const char* const);
void promjenaImenaDatoteci(const char*);
void brisanjeDatoteke(const char*);
int izlazIzPrograma(AUTO*);

#endif //FUNCTIONS_C